﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class Like:Entity
    {
        public string UserId { get; set; }
        public User User { get; set; }
        public Comment Comment { get; set; }
        public int CommentId { get; set; }
    }
}

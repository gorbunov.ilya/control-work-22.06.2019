﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class Topic: Entity
    {
        [Required]
        public string TopicName { get; set; }
        public string AuthorName { get; set; }
        [Required]
        public string TopicText { get; set; }
        public DateTime CreationDate { get; set; }
        public User User { get; set; }
        public string UserId { get; set; }

        public int MessageCount { get; set; }

        public List<Comment> Comments { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class Comment : Entity
    {
        [Required]
        public string Text { get; set; }
        public string UserId { get; set; }
        public User User { get; set; }
        public int TopicId { get; set; }
        public Topic Topic { get; set; }
        public List<Like> Likes { get; set; }

        public DateTime CommentDate { get; set; }
        public string AuthorName { get; set; }
    }
}

﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.Models;
using WebApplication1.Repositories;
using WebApplication1.ViewModels;

namespace WebApplication1.Controllers
{
    public class CommentController : Controller
    {
        private IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly UserManager<User> _userManager;

        public CommentController(IUnitOfWorkFactory unitOfWorkFactory, UserManager<User> userManager)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
            _userManager = userManager;
        }

        public async Task<IActionResult> CreateComment(string text, int topicId, string userId, int pageNumber = 1)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                Comment comment = new Comment()
                {
                    Text = text,
                    TopicId = topicId,
                    UserId = userId,
                    AuthorName = User.Identity.Name,
                    CommentDate = DateTime.Now
                };
                var topic = await unitOfWork.Topics.GetByIdAsync(topicId);
                topic.MessageCount++;
                unitOfWork.Topics.UpdateAsync(topic);
                await unitOfWork.Comments.CreateAsync(comment);
                await unitOfWork.CompleteAsync();
                var comments = unitOfWork.Comments.GetAllAsync().Result.Where(c => c.TopicId == topicId).ToList();
                int pageSize = 5;
                var items = comments.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
                var count = comments.Count;
                PageViewModel pageViewModel = new PageViewModel(count, pageNumber, pageSize);
                var users = _userManager.Users;
                foreach (var item in items)
                {
                    item.User = users.FirstOrDefault(c => c.Id == item.UserId);
                }
                IndexViewModel indexViewModel = new IndexViewModel
                {
                    PageViewModel = pageViewModel,
                    Comments = items
                };
                CommentViewModel commentView = new CommentViewModel
                {

                    Comment = new Comment(),
                    Topic = topic,
                    IndexViewModel = indexViewModel
                };
                return PartialView("CommentsAjax", commentView);
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.Models;
using WebApplication1.Repositories;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        private IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly UserManager<User> _userManager;

        public HomeController(IUnitOfWorkFactory unitOfWorkFactory, UserManager<User> userManager)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
            _userManager = userManager;
        }
        public async Task<IActionResult> Index()
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var topics = await unitOfWork.Topics.GetAllAsync();
                topics.Reverse();
                return View(topics);
            }
            
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}

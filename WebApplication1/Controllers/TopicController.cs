﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.Models;
using WebApplication1.Repositories;
using WebApplication1.ViewModels;

namespace WebApplication1.Controllers
{
    public class TopicController : Controller
    {
        private IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly UserManager<User> _userManager;

        public TopicController(IUnitOfWorkFactory unitOfWorkFactory, UserManager<User> userManager)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
            _userManager = userManager;
        }

        public async Task<ActionResult> Create()
        {
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            if(user !=null)
            ViewBag.UserId = user.Id;
            ViewBag.Date = DateTime.Now;
            return View("~/Views/Topic/Create.cshtml");
        }

        [HttpPost]
        public async Task<ActionResult> CreateTopic(Topic topic)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
               topic.AuthorName = User.Identity.Name;
               await unitOfWork.Topics.CreateAsync(topic);
               await unitOfWork.CompleteAsync();
               return RedirectToAction("Index", "Home");
            }
            
        }

        public async Task<IActionResult> IndexAsync(int id, int pageNumber = 1)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                int pageSize = 5;

                var topic = await unitOfWork.Topics.GetByIdAsync(id);
                var comments = unitOfWork.Comments.GetAllAsync().Result.Where(c => c.TopicId == topic.Id).ToList();
                var count = comments.Count;
                var items = comments.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
                var users = _userManager.Users;
                foreach (var item in items)
                {
                    item.User = users.FirstOrDefault(c => c.Id == item.UserId);
                }
                PageViewModel pageViewModel = new PageViewModel(count, pageNumber, pageSize);
                IndexViewModel indexViewModel = new IndexViewModel
                {
                    PageViewModel = pageViewModel,
                    Comments = items
                };
                CommentViewModel comment = new CommentViewModel() {Comment = new Comment(), Topic = topic, IndexViewModel = indexViewModel};
                return View("Index", comment);
            }
        }

        public async Task<IActionResult> PaginationAjaxResultAsync(int id, int pageNumber)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                int pageSize = 5;

                var topic = await unitOfWork.Topics.GetByIdAsync(id);
                var comments = unitOfWork.Comments.GetAllAsync().Result.Where(c => c.TopicId == topic.Id).ToList();
                var items = comments.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
                var count = comments.Count;
                var users = _userManager.Users;
                foreach (var item in items)
                {
                    item.User = users.FirstOrDefault(c => c.Id == item.UserId);
                }
                PageViewModel pageViewModel = new PageViewModel(count, pageNumber, pageSize);
                IndexViewModel indexViewModel = new IndexViewModel
                {
                    PageViewModel = pageViewModel,
                    Comments = items
                };
                CommentViewModel comment = new CommentViewModel {

                    Comment = new Comment(),
                    Topic = topic,
                    IndexViewModel = indexViewModel
                };
                return PartialView("PaginationResult", comment);
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using WebApplication1.Models;

namespace WebApplication1.Data
{
    public class ApplicationDbContext : IdentityDbContext<User>
    {
        public DbSet<Topic> Topics { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Like> Likes { get; set; }
        public ApplicationDbContext(DbContextOptions options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Models.Topic>()
                .HasOne(c => c.User)
                .WithMany(p => p.Topics)
                .HasForeignKey(k => k.UserId);

            modelBuilder.Entity<Models.Comment>()
                .HasOne(c => c.Topic)
                .WithMany(p => p.Comments)
                .HasForeignKey(k => k.TopicId);

            modelBuilder.Entity<Models.Comment>()
                .HasOne(c => c.User)
                .WithMany(p => p.Comments)
                .HasForeignKey(k => k.UserId);


            modelBuilder.Entity<Like>()
                .HasKey(bc => new { bc.CommentId, bc.UserId });

            modelBuilder.Entity<Like>()
                .HasOne(bc => bc.Comment)
                .WithMany(b => b.Likes)
                .HasForeignKey(bc => bc.CommentId);

            modelBuilder.Entity<Like>()
                .HasOne(bc => bc.User)
                .WithMany(c => c.Likes)
                .HasForeignKey(bc => bc.UserId);
        }
    }
}

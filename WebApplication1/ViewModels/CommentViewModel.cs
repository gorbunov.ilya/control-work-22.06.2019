﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Models;

namespace WebApplication1.ViewModels
{
    public class CommentViewModel
    {
        public Comment Comment { get; set; }
        public Topic Topic { get; set; }

        public IndexViewModel IndexViewModel { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Data;

namespace WebApplication1.Repositories.Like
{
    public class LikeRepository : Repository<Models.Like>, ILikeRepository
    {
        public LikeRepository(ApplicationDbContext context) : base(context)
        {
        }
    }

    public interface ILikeRepository : IRepository<Models.Like>
    {
    }
}

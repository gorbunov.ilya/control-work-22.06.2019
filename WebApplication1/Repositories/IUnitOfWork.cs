﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Models;
using WebApplication1.Repositories.Comment;
using WebApplication1.Repositories.Like;
using WebApplication1.Repositories.Topic;

namespace WebApplication1.Repositories
{
    public interface IUnitOfWork : IDisposable
    {

        ICommentRepository Comments { get; }
        ITopicRepository Topics { get; }
        ILikeRepository Likes { get; }

        Task<int> CompleteAsync();

        IRepository<TEntity> GetRepository<TEntity>() where TEntity : Entity;

    }
}

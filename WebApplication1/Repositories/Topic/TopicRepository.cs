﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Data;

namespace WebApplication1.Repositories.Topic
{
    public class TopicRepository : Repository<Models.Topic>, ITopicRepository
    {
        public TopicRepository(ApplicationDbContext context) : base(context)
        {
        }
    }

    public interface ITopicRepository : IRepository<Models.Topic>
    {
    }
}

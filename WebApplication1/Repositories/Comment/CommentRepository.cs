﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Data;

namespace WebApplication1.Repositories.Comment
{
    public class CommentRepository : Repository<Models.Comment>, ICommentRepository
    {
        public CommentRepository(ApplicationDbContext context) : base(context)
        {
        }
    }

    public interface ICommentRepository : IRepository<Models.Comment>
    {
    }
}
